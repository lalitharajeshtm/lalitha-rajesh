﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5
{
    public class Modelclass
    {
        public int id { get; set; }
        public string cust_name { get; set; }
        public string address { get; set; }
        public int phone { get; set; }
        public string email { get; set; }
        public string department { get; set; }
    }
}